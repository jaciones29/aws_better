# frozen_string_literal: true

require 'thor'

module AwsBetter
  module Commands
    class Lambda < Thor

      namespace :lambda

      desc 'ls', 'Command description...'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def ls(*)
        if options[:help]
          invoke :help, ['ls']
        else
          require_relative 'lambda/ls'
          AwsBetter::Commands::Lambda::Ls.new(options).execute
        end
      end
    end
  end
end
