# frozen_string_literal: true

require_relative '../../command'

module AwsBetter
  module Commands
    class Ec2
      class UnusedSnapshots < AwsBetter::Command
        def initialize(options)
          @options = options
        end

        def get_amis
          client = get_ec2_client
          response = client.describe_images(owners: ['self'])
          response.to_h[:images]
        end

        def get_snapshots
          client = get_ec2_client
          response = client.describe_snapshots(owner_ids: ['self'])
          response.to_h[:snapshots]
        end

        def get_snapshot_ids_from_images(amis)
          snapshot_ids = []
          amis.each do |image|
            next unless image[:block_device_mappings]

            image[:block_device_mappings].each do |image_bdm|
              if image_bdm[:ebs]
                ebs = image_bdm[:ebs]
                snapshot_ids << ebs[:snapshot_id]
              end
            end
          end
          snapshot_ids
        end

        def build_info_table(snapshots)
          pastel = Pastel.new
          rows = []
          snapshots.each do |s|
            rows << s[:snapshot_id] + ' :: ' + pastel.cyan(s[:description])
          end
          rows
        end

        def get_unused
          amis = get_amis
          snapshots = get_snapshots

          ami_snapshot_ids = get_snapshot_ids_from_images(amis)
          existing_snapshot_ids = snapshots.map { |s| s[:snapshot_id] }

          unused_snapshot_ids = existing_snapshot_ids - ami_snapshot_ids

          snapshots.keep_if { |e| unused_snapshot_ids.include?(e[:snapshot_id]) }
        end

        def execute(input: $stdin, output: $stdout)
          # Command logic goes here ...
          puts 'Loading...'
          unused = get_unused
          return if unused.empty?

          puts ''

          pastel = Pastel.new
          puts pastel.red("There are #{unused.size} unused snapshots")
          if prompt.yes?('Do you wish to delete unused snapshots?')
            snapshots = prompt.multi_select("Select unused snapshots to delete?(#{unused.size})", build_info_table(unused), per_page: 20, echo: false)
            delete_multi(snapshots)
          end
        end

        def delete_multi(snapshots)
          client = get_ec2_client

          snapshots.each do |snapshot|
            id = snapshot.split(' :: ')[0]
            resp = client.delete_snapshot(snapshot_id: id)
            puts "...deleted #{id}"
          end
        end
      end
    end
  end
end
