# frozen_string_literal: true

require_relative '../../command'

module AwsBetter
  module Commands
    class Ec2
      class UnusedReserved < AwsBetter::Command
        def initialize(options)
          @options = options
        end

        def running_instances(instances, token = nil)
          client = get_ec2_client
          resp = client.describe_instances(build_running_filter)
          reservations = resp.reservations
          reservations.each do |res|
            instances.append(res.instances)
          end
          next_token = resp.next_token
          return if next_token.nil?

          running_instances(instances, next_token)
        end

        def reservations
          client = get_ec2_client

          reserved_types = []
          resp = client.describe_reserved_instances(build_reserved_filter)
          reserved = resp.reserved_instances

          reserved.each do |r|
            r.instance_count.times do 
              reserved_types << r.instance_type
            end
          end
          tally = reserved_types.inject(Hash.new(0)) { |total, e| total[e] += 1 ;total}
          tally
        end

        def build_running_filter
          {
            filters: [
              {
                name: "instance-state-name",
                values: ["running"]
              }
            ]
          }
        end

        def build_reserved_filter
          {
            filters: [
              {
                name: "state",
                values: ["active"]
              }
            ]
          }
        end

        def build_running_tally(running)
          types = []
          running.each do |instance|
            instance.each do |i|
              types << i.instance_type
            end
          end
          tally = types.inject(Hash.new(0)) { |total, e| total[e] += 1 ;total}
          tally
        end

        def show_non_reserved_instances(running_tally, reserved_tally)
          pastel = Pastel.new
          counts = {}
          all_keys = (running_tally.keys + reserved_tally.keys).uniq
          all_keys.sort.each do |key|
            counts[key] = running_tally[key].to_i - reserved_tally[key].to_i
          end
          counts.each do |k,v|
            state = "properly reserved"
            if v > 0
              state = "needs #{v} new reservations"
            elsif v < 0
              state = "#{-v} reseved instances NOT used"
            end

            puts pastel.inverse(k) + " #{state}"
          end
        end


        def execute(input: $stdin, output: $stdout)
          instances = []
          # Recursive instance builder
          running_instances(instances)

          running_tally = build_running_tally(instances)
          reserved_tally = reservations
          show_non_reserved_instances(running_tally, reserved_tally)
        end
      end
    end
  end
end
