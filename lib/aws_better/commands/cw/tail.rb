# frozen_string_literal: true

require_relative '../../command'
require_relative 'tail_history'
require 'pastel'

module AwsBetter
  module Commands
    class Cw
      class Tail < AwsBetter::Command
        SAMPLE_TIME = 5
        def initialize(name = nil, options)
          @name = name
          @options = options
        end

        def color(event_message, pastel)
          if event_message.include?("END RequestId:") || event_message.include?("REPORT RequestId:") || event_message.include?("START RequestId:")
            return pastel.blue(event_message)
          end

          if event_message.include?("Error") || event_message.include?("Exception") || event_message.include?("Failed")
            return pastel.red(event_message)
          end

          return event_message
        end

        def execute(input: $stdin, output: $stdout)
          pastel = Pastel.new
          # Command logic goes here ...
          data = get_log_groups
          if @name.nil?
            log_group = prompt.select('Which log?', data, per_page: 20)
          else
            log_group = @name
            unless data.include?(@name)
              text = pastel.red("#{@name} not found in log streams. You need the WHOLE log stream name")
              puts text
              ok_prompt = "I will now list them for you, you should copy the full name from the list, OK?"
              if prompt.yes?(ok_prompt)
                log_group = prompt.select('Which log?', data, per_page: 20)
              else
                exit 0
              end
            end
            puts ""
            puts pastel.inverse("Following -> #{log_group}")
          end
          tail_history = TailHistory.new
          loop do
            streams = get_log_streams(log_group)
            post_latest(streams, log_group, tail_history)
            new_items = tail_history.latest
            new_items.each do |event|
              f = event.timestamp.to_f / 1000.0
              time = Time.at(f)
              formatted_time = time.strftime("%a %-d, %H:%M:%S")
              event_message = color(event.message, pastel)

              puts pastel.green(formatted_time) + " " + event_message
            end
            sleep(SAMPLE_TIME)
          end
        end

        def post_latest(streams, log_group, tail_history)
          client = get_cloudwatch_logs_client
          streams.each do |stream|
            events = []
            recurse_events(client, events, log_group, stream)
            if events.empty? && tail_history.last_check.nil?
              get_last_x(client, events, log_group, stream)
            end
            tail_history.add_events(events)
          end
        end

        def recurse_events(client, events, log_group, stream, next_token = nil)
          config = {
            log_group_name: log_group,
            log_stream_name: stream,
            start_time: (Time.now - (SAMPLE_TIME * 20)).to_i * 1000,
            start_from_head: false
          }
          config[:next_token] = next_token if next_token
          resp = client.get_log_events(config)
          resp.events.each do |event|
            events << event
          end

          # We're not going to recurse here because we are starting 60 seconds ago and reload. The AWS API
          # here is wonky.
        end

        def get_last_x(client, events, log_group, stream, x = 20)
          config = {
            log_group_name: log_group,
            log_stream_name: stream,
            start_from_head: false,
            limit: x
          }
          resp = client.get_log_events(config)
          resp.events.each do |event|
            events << event
          end
        end

        def get_log_groups(prefix = nil)
          cw_client = get_cloudwatch_logs_client
          results = []
          recurse_log_groups(cw_client, results, prefix)
          results
        end

        def recurse_log_groups(client, results, prefix, next_token = nil)
          config = {
            next_token: next_token,
            limit: 50
          }
          config[:log_group_name_prefix] = prefix if prefix
          resp = client.describe_log_groups(config)
          groups = resp.log_groups
          groups.each do |group|
            results << group.log_group_name
          end
          next_token = resp.next_token
          recurse_log_groups(client, results, prefix, next_token) if next_token
        end

        def get_log_streams(log_group)
          client = get_cloudwatch_logs_client
          config = {
            log_group_name: log_group,
            order_by: 'LastEventTime',
            descending: true,
            limit: 50
          }
          resp = client.describe_log_streams(config)
          collection = []
          parse_log_streams_until_not_latest(client, config, resp, collection)
          collection
        end

        def parse_log_streams_until_not_latest(client, config, resp, collection)
          resp.log_streams.each do |stream|
#            puts stream.inspect
            if stream.last_ingestion_time.nil? || stream.last_ingestion_time < (Time.now.utc - 4800).to_i * 1000
              return
            end

            collection << stream.log_stream_name
          end
          config[:next_token] = resp.next_token
          resp = client.describe_log_streams(config)
          parse_log_streams_until_not_latest(client, config, resp, collection) if resp.next_token
        end
      end
    end
  end
end
