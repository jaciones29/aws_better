class TailHistory

  attr_reader :last_check

  def initialize
    @last_check = nil
    @data = {}
  end

  def add_events(events)
    # resp.events[0].timestamp #=> Integer
    # resp.events[0].message #=> String
    # resp.events[0].ingestion_time #=> Intege
    events.each do |event|
      # We are going to filter out exact match events
      hash_lookup = build_key(event)
      @data[hash_lookup] = event
    end
  end

  def latest
    ordered_keys = @data.keys.sort
    # Get last 20 if none already loaded
    if @last_check.nil?
      e = ordered_keys.last(20)[0]
      @last_check = build_key(@data[e]) if e
    end

    new_keys = ordered_keys.select {|k| k.to_s > @last_check.to_s }
    results = []
    new_keys.each do |k|
      results << @data[k]
      @last_check = build_key(@data[k])
    end
    results
  end

  def build_key(event)
    event.ingestion_time.to_s + "^" + event.message.to_s
  end
end