# frozen_string_literal: true

module AwsBase
  def get_recursive_items(client, base_config, function_name, next_token, &block)
    config = base_config.clone
    config[:next_token] = next_token if next_token
    resp = client.send(function_name, config)
    yield resp if resp
    get_recursive_items(client, base_config, function_name, resp.next_token, block) if resp&.next_token
  end

  def get_recursive_items_by_marker(client, base_config, function_name, next_token, &block)
    config = base_config.clone
    config[:marker] = next_token if next_token
    resp = client.send(function_name, config)
    yield resp if resp
    get_recursive_items_by_marker(client, base_config, function_name, resp.next_marker, block) if resp&.next_marker
  end

end
