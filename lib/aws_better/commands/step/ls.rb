# frozen_string_literal: true

require_relative '../../command'
require_relative 'step_base'

module AwsBetter
  module Commands
    class Step
      class Ls < AwsBetter::Command
        include StepBase

        def initialize(options)
          @options = options
        end

        def execute(input: $stdin, output: $stdout)
          # Command logic goes here ...
          machines = all_state_machines
          display(machines)
        end

        def display(machines)
          pastel = Pastel.new

          rows = []
          machines.each do |machine|
            name = pastel.yellow(machine[:name])

            status = machine[:latest_execution] ? machine[:latest_execution].status : "-"
            if status == 'SUCCEEDED'
              status = pastel.green(status)
            elsif status == 'ABORTED' || status == 'FAILED'
              status = pastel.red(status)
            end
            duration = '-'
            if machine[:latest_execution] && machine[:latest_execution].stop_date
              stop_date = Time.parse(machine[:latest_execution].stop_date.to_s)
              day = get_day_display(stop_date, pastel)
              duration = get_duration_display(machine[:latest_execution].stop_date - machine[:latest_execution].start_date)

            end
            rows << [name, status, duration, day]
          end
          puts ''
          t = tty_table(header: ['Step Function Name', 'Last Result', 'Duration', 'Last Result Date'], rows: rows)
          puts t.render(:unicode)
        end

        def get_duration_display(seconds)
          hours = Time.at(seconds).utc.strftime('%H')
          hours = hours.to_i.zero? ? '' : "#{hours.to_i}:"
          icon = ''

          minutes = Time.at(seconds).utc.strftime('%M')
          if hours.to_i.zero?
            minutes = if minutes.to_i.zero?
                        Time.at(seconds).utc.strftime('%s') + ' sec'
                      else
                        "#{minutes.to_i} min"
                      end
          else
            icon = 'h:m'
          end

          "#{hours}#{minutes} #{icon}"
        end

        def get_day_display(stop_date, pastel)
          day = '%a'
          day = pastel.cyan('Today') if stop_date.to_date == Date.today
          day = pastel.cyan('Yesterday') if stop_date.to_date == Date.today - 1
          day = '%b %d' if (Date.today - stop_date.to_date) > 7
          day = '%b %d,%Y  ' if (Date.today - stop_date.to_date) > 360
          timezone = pastel.blue(stop_date.strftime('%Z'))
          stop_date.strftime("#{day} at %I:%M%p #{timezone}")
        end
      end
    end
  end
end
