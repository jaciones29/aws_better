# frozen_string_literal: true

require_relative '../../command'
require_relative 'step_base'

module AwsBetter
  module Commands
    class Step
      class Tasks < AwsBetter::Command
        include StepBase

        def initialize(options)
          @options = options
        end

        def execute(input: $stdin, output: $stdout)
          # Command logic goes here ...
          state_machines = state_machines_info
          parse_definitions(state_machines)
          display_state_machines(state_machines)
        end

        def display_state_machines(state_machines)
          puts
          pastel = Pastel.new
          state_machines.each do |machine|
            rows = build_info_table(machine[:tasks])
            name = pastel.yellow(machine[:name])
            puts name
            rows.each do |row|

              puts pastel.green(row[0]) + ' ' + row[1] + ' (' + pastel.italic(row[2]) + ')'
            end

            puts ''
          end
        end

        def build_info_table(info)
          rows = []
          info.each do |key, value|
            if value["Type"] == "Task"
              params = value["Parameters"]
              if params && params["TaskDefinition"]
                rows << [params["TaskDefinition"], params["LaunchType"], params["Cluster"]]
              end
            end
          end
          rows
        end

        def parse_definitions(state_machines)
          state_machines.each do |state_machine|
            definition = state_machine[:info].definition
            state_machine[:tasks] = get_tasks_from_definitioon(definition)
          end
        end

        def get_tasks_from_definitioon(definition)
          tasks = {}
          json = JSON.parse(definition)
          json['States'].each do |_name, content|
            if content['Type'] == 'Task'
              parameters = content['Parameters']
              tasks['name'] = parameters
            end
          end
        end
      end
    end
  end
end
