# frozen_string_literal: true

require_relative '../aws_base'

# Base class for Step Functions
module StepBase
  include AwsBase

  def state_machines_info
    client = get_states_client
    machines = []
    get_machines(client, machines)
    print 'Loading...'
    machines.each do |machine|
      print '.'
      STDOUT.flush
      info = get_machine_info(machine[:arn])
      machine[:info] = info
    end
  end

  def all_state_machines
    client = get_states_client
    machines = []
    get_machines(client, machines)
    print 'Loading...'

    machines.each do |machine|
      print '.'
      STDOUT.flush
      latest = get_lastest_execution(machine[:arn])
      machine[:latest_execution] = latest
    end
    machines
  end

  def get_machines(client, machines, _next_token = nil)
    base_config = {
      max_results: 100
    }
    get_recursive_items(client, base_config, 'list_state_machines', nil) do |resp|
      if resp&.state_machines
        resp.state_machines.each do |machine|
          machines << { arn: machine.state_machine_arn, name: machine.name }
        end
      end
    end
  end

  def get_machine_info(arn)
    client = get_states_client
    resp = client.describe_state_machine(
      state_machine_arn: arn
    )
    resp
  end

  def get_lastest_execution(arn)
    client = get_states_client
    executions = []
    base_config = {
      state_machine_arn: arn,
      max_results: 1
    }

    resp = client.list_executions(base_config)
    if resp&.executions
      resp.executions.each do |execution|
        executions << execution
      end
    end

    # Build all executions...(in case we need something like this later)
    executions.sort! { |a, b| a.stop_date <=> b.stop_date }
    # But return only the latest one
    executions[0]
  end
end
