# frozen_string_literal: true

require_relative 'aws_base'
require_relative '../command'

module AwsBetter
  module Commands
    class Config < AwsBetter::Command
      def initialize(options)
        @options = options
      end

      def execute(input: $stdin, output: $stdout)
        # Command logic goes here ...
        output.puts "OK"
      end
    end
  end
end
