# frozen_string_literal: true

require_relative '../../command'
require_relative 'lambda_base'

module AwsBetter
  module Commands
    class Lambda
      class Ls < AwsBetter::Command
        include LambdaBase

        def initialize(options)
          @options = options
        end

        def execute(input: $stdin, output: $stdout)
          lambdas = all_lambdas
          lambdas.sort! { |a, b| a[:name] <=> b[:name] }
          display(lambdas)
        end

        def display(lambdas)
          pastel = Pastel.new

          rows = []
          lambdas.each do |lambda|
            name = pastel.yellow(lambda[:name])
            time_ago = lambda[:time_ago] || 'a long time ago'
            errors = lambda[:errors]

            rows << [name, time_ago, errors.to_i]
          end
          puts ''
          t = tty_table(header: ['Lambda Function Name', 'Last Run', 'Recent Errors'], rows: rows)
          puts t.render(:unicode)
        end
      end
    end
  end
end
