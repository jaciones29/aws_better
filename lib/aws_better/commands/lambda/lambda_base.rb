# frozen_string_literal: true

require_relative '../aws_base'
require 'dotiw'
#require 'action_view'
#require 'action_view/helpers'
include ActionView::Helpers::DateHelper

module LambdaBase
  include AwsBase
  MINUTE = 60
  HOUR = MINUTE * 60
  DAY = HOUR * 24
  WEEK = DAY * 7
  MONTH = DAY * 30
  YEAR = MONTH * 12

  def all_lambdas
    client = get_lambda_client
    lambdas = []
    get_functions(client, lambdas)
    #    print 'Loading...'
    #    puts lambdas.inspect

    lambdas.each do |lambda|
      print '.'
      STDOUT.flush
      last = query_cloudwatch_daily(lambda[:name])
      errors = query_cloudwatch(lambda[:name], DAY, DAY, 'Errors')
      lambda[:time_ago] = distance_of_time_in_words(Time.now.utc, last.timestamp, include_seconds: true) + ' ago' if last&.timestamp
      
      lambda[:errors] = errors ? errors["sum"] : 0
      lambda[:errors] ||=0

      sleep(0.2)
    end
    lambdas
  end

  def get_functions(client, lambdas, _next_token = nil)
    base_config = {
      max_items: 50
    }
    get_recursive_items_by_marker(client, base_config, 'list_functions', nil) do |resp|
      if resp&.functions
        resp.functions.each do |function|
          lambdas << { name: function.function_name, function_arn: function.function_arn }
        end
      end
    end
  end

  def query_cloudwatch_daily(function_name)
    last = query_cloudwatch(function_name, WEEK, DAY)
    return nil if last.nil?

    return query_cloudwatch_monthly(function_name) if Time.now.utc.to_i - last.timestamp.to_i > DAY + 60

    query_cloudwatch_hourly(function_name)
  end

  def query_cloudwatch_hourly(function_name)
    last = query_cloudwatch(function_name, DAY, HOUR)
    return last if Time.now.utc.to_i - last.timestamp.to_i > HOUR + 60

    query_cloudwatch(function_name, HOUR, MINUTE)
  end 

  def query_cloudwatch_monthly(function_name)
    last = query_cloudwatch(function_name, YEAR, WEEK)
    return nil if last.nil?

    last
  end

  def query_cloudwatch(function_name, distance, period, name = 'Invocations')
    cw_client = get_cloudwatch_client
    params = {
      namespace: 'AWS/Lambda', # required
      metric_name: name, # required
      dimensions: [
        {
          name: 'FunctionName', # required
          value: function_name # required
        }
      ],
      statistics: ['Sum'],
      start_time: Time.now - distance, # required
      end_time: Time.now, # required
      period: period
    }
    sorted_array = []
    cw_client.get_metric_statistics(params).datapoints.each do |dp|
      sorted_array << dp if dp.sum.to_i > 0
    end
    sorted_array.sort! { |a, b| a.timestamp <=> b.timestamp }
    sorted_array.last
  end
end
