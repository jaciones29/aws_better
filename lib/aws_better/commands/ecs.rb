# frozen_string_literal: true

require 'thor'

module AwsBetter
  module Commands
    class Ecs < Thor

      namespace :ecs

      desc 'cleanup', 'Command description...'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def cleanup(*)
        if options[:help]
          invoke :help, ['cleanup']
        else
          require_relative 'ecs/cleanup'
          AwsBetter::Commands::Ecs::Cleanup.new(options).execute
        end
      end

      desc 'ls', 'Command description...'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def ls(*)
        if options[:help]
          invoke :help, ['ls']
        else
          require_relative 'ecs/ls'
          AwsBetter::Commands::Ecs::Ls.new(options).execute
        end
      end
    end
  end
end
