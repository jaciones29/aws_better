# frozen_string_literal: true

require_relative '../../command'
require_relative 'ecs_base'

module AwsBetter
  module Commands
    class Ecs
      class Cleanup < AwsBetter::Command
        include EcsBase

        TaskItem = Struct.new(:name, :version)

        def initialize(options)
          @options = options
        end

        def deregister(task)
          puts "Deregistering #{task}"
          client = get_ecs_client

          resp = client.deregister_task_definition({
            task_definition: task
          })
          puts "Removed #{task}"
        end

        def build_menu_items_first(sorted_array_of_versions)
          menu_items = []
          i = 0
          i_set = []
          subitems = sorted_array_of_versions.clone
          until subitems.empty?
            i += 1
            item = subitems.shift
            menu_items << [item.name + ":" + item.version.to_s , i]
            i_set << i unless subitems.empty?
          end

          return menu_items, i_set
        end


        def execute(input: $stdin, output: $stdout)
          tasks = all_ecs_task_definitions
          dups = find_tasks_with_multiple_versions(tasks)
          i = 0
          i_set = []

          dups.each do |key, sorted_array_of_versions|
            menu_items, i_set = build_menu_items_first(sorted_array_of_versions)
            results = prompt.multi_select('Select Versions to Deregister?', per_page: 10, echo: false) do |menu|
              menu.default *i_set
              menu_items.each do |menu_item|
                menu.choice menu_item[0], menu_item[1]
              end
            end
            results.each do |index|
              deregister(menu_items[index-1][0])
            end

          end
        end

        def find_tasks_with_multiple_versions(tasks)
          lookup = []
          dups = []
          tasks.each do |task|
            match = task.match(/(^.*)\:([0-9]*)$/)
            name = match[1]
            if lookup.include?(name)
              dups << name
            else
              lookup << name
            end
          end

          task_versions = {}
          dups.each do |dup|
            items = []
            name = ''
            rows = tasks.select { |x| x.start_with?(dup + ':') }
            rows.each do |task|
              match = task.match(/(^.*)\:([0-9]*)$/)
              name = match[1]
              version = match[2].to_i
              items << TaskItem.new(name, version)
            end
            items.sort! { |a, b| a.version <=> b.version }
            task_versions[name] = items
          end
          task_versions
        end
      end
    end
  end
end
