# frozen_string_literal: true

require_relative '../../command'
require_relative 'ecs_base'

module AwsBetter
  module Commands
    class Ecs
      class Ls < AwsBetter::Command
        include EcsBase

        def initialize(options)
          @options = options
        end

        def execute(input: $stdin, output: $stdout)
          # Command logic goes here ...
          results = {}
          tasks = all_ecs_task_definitions
          puts tasks.inspect
          tasks.each do |t|
            puts t
            results[t] = get_task_definition_by_id(t)
          end
          puts results.inspect
            display_all(results)

#          task = prompt.select('Which task?', tasks, per_page: 30)
#          results[task] = get_task_definition_by_id(task)
 #         display_all(results)
        end

        def build_log_row(pastel, log_configuration)
          return '' unless log_configuration
          driver = pastel.italic.yellow("LOG: ") + log_configuration.log_driver + ' '
          if log_configuration.options.dig("awslogs-group")
            driver += pastel.italic.yellow("CLOUD_WATCH: ") + log_configuration.options.dig("awslogs-group")
          else
            driver += pastel.italic.yellow("OPTIONS: ") + "#{log_configuration.options.to_json}"
          end
          driver
        end

        def display_all(tasks)
          pastel = Pastel.new
          tasks.keys.sort.each do |name|
            data = tasks[name]
            puts pastel.inverse("TASK:" + name)
            data.each do |container|
              image_name = container.image
              display_name = container.name
              command = container.command
              puts pastel.italic.yellow("IMAGE: ") + image_name + pastel.blue(" (#{display_name})")
              puts pastel.italic.yellow("CMD: ") + command.to_s unless command.to_s.empty?
              puts build_log_row(pastel, container.log_configuration)
              puts pastel.italic.yellow("ENV: ") + container.environment.to_s
            end

            puts '',''
          end
        end
      end
    end
  end
end
