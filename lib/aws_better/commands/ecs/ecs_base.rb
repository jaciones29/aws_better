# frozen_string_literal: true

module EcsBase
  def all_ecs_task_definitions
    client = get_ecs_client
    tasks = []
    ecs_tasks(client, tasks)
    tasks
  end

  def ecs_tasks(client, tasks, next_token = nil)
    config = {
      status: 'ACTIVE',
      sort: 'ASC',
      max_results: 50
    }
    config[:next_token] = next_token if next_token
    resp = client.list_task_definitions(config)

    if resp&.task_definition_arns
      resp.task_definition_arns.each do |arn|
        tasks << arn
      end
    end

    ecs_tasks(client, tasks, resp.next_token) if resp.next_token
  end

  def get_task_definition_by_id(id)
    client = get_ecs_client
    resp = client.describe_task_definition(
      task_definition: id
    )
    if resp && resp.task_definition && resp.task_definition.container_definitions
      return resp.task_definition.container_definitions
    end

    []
  end
end
