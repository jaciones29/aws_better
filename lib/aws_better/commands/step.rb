# frozen_string_literal: true

require 'thor'

module AwsBetter
  module Commands
    class Step < Thor

      namespace :step

      desc 'tasks', 'Command description...'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def tasks(*)
        if options[:help]
          invoke :help, ['tasks']
        else
          require_relative 'step/tasks'
          AwsBetter::Commands::Step::Tasks.new(options).execute
        end
      end

      desc 'ls', 'Command description...'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def ls(*)
        if options[:help]
          invoke :help, ['ls']
        else
          require_relative 'step/ls'
          AwsBetter::Commands::Step::Ls.new(options).execute
        end
      end
    end
  end
end
