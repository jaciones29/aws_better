# frozen_string_literal: true

require 'thor'

module AwsBetter
  module Commands
    class Cw < Thor

      namespace :cw

      desc 'tail', 'Get live cloudwatch output'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def tail(name = nil)
        if options[:help]
          invoke :help, ['tail']
        else
          require_relative 'cw/tail'
          AwsBetter::Commands::Cw::Tail.new(name, options).execute
        end
      end
    end
  end
end
