# frozen_string_literal: true

require 'thor'

module AwsBetter
  module Commands
    class Ec2 < Thor

      namespace :ec2

      desc 'unused_reserved', 'Command description...'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def unused_reserved(*)
        if options[:help]
          invoke :help, ['unused_reserved']
        else
          require_relative 'ec2/unused_reserved'
          AwsBetter::Commands::Ec2::UnusedReserved.new(options).execute
        end
      end

      desc 'unused_snapshots', 'Command description...'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def unused_snapshots(*)
        if options[:help]
          invoke :help, ['unused_snapshots']
        else
          require_relative 'ec2/unused_snapshots'
          AwsBetter::Commands::Ec2::UnusedSnapshots.new(options).execute
        end
      end
    end
  end
end
