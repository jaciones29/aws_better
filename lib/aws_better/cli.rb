# frozen_string_literal: true

require 'thor'

module AwsBetter
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    desc 'version', 'aws_better version'
    def version
      require_relative 'version'
      puts "v#{AwsBetter::VERSION}"
    end
    map %w(--version -v) => :version

    require_relative 'commands/ec2'
    register AwsBetter::Commands::Ec2, 'ec2', 'ec2 [SUBCOMMAND]', 'Command description...'

    require_relative 'commands/lambda'
    register AwsBetter::Commands::Lambda, 'lambda', 'lambda [SUBCOMMAND]', 'Command description...'

    require_relative 'commands/step'
    register AwsBetter::Commands::Step, 'step', 'step [SUBCOMMAND]', 'Command description...'

    require_relative 'commands/ecs'
    register AwsBetter::Commands::Ecs, 'ecs', 'ecs [SUBCOMMAND]', 'Command description...'

    require_relative 'commands/cw'
    register AwsBetter::Commands::Cw, 'cw', 'cw [SUBCOMMAND]', 'Get live cloudwatch output'

    desc 'config', 'Command description...'
    method_option :help, aliases: '-h', type: :boolean,
                         desc: 'Display usage information'
    def config(*)
      if options[:help]
        invoke :help, ['config']
      else
        require_relative 'commands/config'
        AwsBetter::Commands::Config.new(options).execute
      end
    end
  end
end
