RSpec.describe "`aws_better ecs` command", type: :cli do
  it "executes `aws_better help ecs` command successfully" do
    output = `aws_better help ecs`
    expected_output = <<-OUT
Commands:
    OUT

    expect(output).to eq(expected_output)
  end
end
