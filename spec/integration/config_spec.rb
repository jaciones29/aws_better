RSpec.describe "`aws_better config` command", type: :cli do
  it "executes `aws_better help config` command successfully" do
    output = `aws_better help config`
    expected_output = <<-OUT
Usage:
  aws_better config

Options:
  -h, [--help], [--no-help]  # Display usage information

Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
