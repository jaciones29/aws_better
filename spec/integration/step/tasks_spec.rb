RSpec.describe "`aws_better step tasks` command", type: :cli do
  it "executes `aws_better step help tasks` command successfully" do
    output = `aws_better step help tasks`
    expected_output = <<-OUT
Usage:
  aws_better tasks

Options:
  -h, [--help], [--no-help]  # Display usage information

Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
