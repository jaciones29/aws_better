RSpec.describe "`aws_better cw` command", type: :cli do
  it "executes `aws_better help cw` command successfully" do
    output = `aws_better help cw`
    expected_output = <<-OUT
Commands:
    OUT

    expect(output).to eq(expected_output)
  end
end
