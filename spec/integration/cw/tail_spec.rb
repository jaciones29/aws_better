RSpec.describe "`aws_better cw tail` command", type: :cli do
  it "executes `aws_better cw help tail` command successfully" do
    output = `aws_better cw help tail`
    expected_output = <<-OUT
Usage:
  aws_better tail

Options:
  -h, [--help], [--no-help]  # Display usage information

Get live cloudwatch output
    OUT

    expect(output).to eq(expected_output)
  end
end
