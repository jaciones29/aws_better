RSpec.describe "`aws_better lambda` command", type: :cli do
  it "executes `aws_better help lambda` command successfully" do
    output = `aws_better help lambda`
    expected_output = <<-OUT
Commands:
    OUT

    expect(output).to eq(expected_output)
  end
end
