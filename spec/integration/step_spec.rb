RSpec.describe "`aws_better step` command", type: :cli do
  it "executes `aws_better help step` command successfully" do
    output = `aws_better help step`
    expected_output = <<-OUT
Commands:
    OUT

    expect(output).to eq(expected_output)
  end
end
