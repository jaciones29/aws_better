RSpec.describe "`aws_better ecs cleanup` command", type: :cli do
  it "executes `aws_better ecs help cleanup` command successfully" do
    output = `aws_better ecs help cleanup`
    expected_output = <<-OUT
Usage:
  aws_better cleanup

Options:
  -h, [--help], [--no-help]  # Display usage information

Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
