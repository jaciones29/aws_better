RSpec.describe "`aws_better ecs ls` command", type: :cli do
  it "executes `aws_better ecs help ls` command successfully" do
    output = `aws_better ecs help ls`
    expected_output = <<-OUT
Usage:
  aws_better ls

Options:
  -h, [--help], [--no-help]  # Display usage information

Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
