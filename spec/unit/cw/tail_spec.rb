require 'aws_better/commands/cw/tail'

RSpec.describe AwsBetter::Commands::Cw::Tail do
  it "executes `cw tail` command successfully" do
    output = StringIO.new
    options = {}
    command = AwsBetter::Commands::Cw::Tail.new(options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
