require 'aws_better/commands/ecs/ls'

RSpec.describe AwsBetter::Commands::Ecs::Ls do
  it "executes `ecs ls` command successfully" do
    output = StringIO.new
    options = {}
    command = AwsBetter::Commands::Ecs::Ls.new(options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
