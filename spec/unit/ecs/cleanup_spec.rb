require 'aws_better/commands/ecs/cleanup'

RSpec.describe AwsBetter::Commands::Ecs::Cleanup do
  it "executes `ecs cleanup` command successfully" do
    output = StringIO.new
    options = {}
    command = AwsBetter::Commands::Ecs::Cleanup.new(options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
