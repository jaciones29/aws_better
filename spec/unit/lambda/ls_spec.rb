require 'aws_better/commands/lambda/ls'

RSpec.describe AwsBetter::Commands::Lambda::Ls do
  it "executes `lambda ls` command successfully" do
    output = StringIO.new
    options = {}
    command = AwsBetter::Commands::Lambda::Ls.new(options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
