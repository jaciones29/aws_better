require 'aws_better/commands/step/tasks'

RSpec.describe AwsBetter::Commands::Step::Tasks do
  it "executes `step tasks` command successfully" do
    output = StringIO.new
    options = {}
    command = AwsBetter::Commands::Step::Tasks.new(options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
