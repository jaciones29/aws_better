require 'aws_better/commands/step/ls'

RSpec.describe AwsBetter::Commands::Step::Ls do
  it "executes `step ls` command successfully" do
    output = StringIO.new
    options = {}
    command = AwsBetter::Commands::Step::Ls.new(options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
