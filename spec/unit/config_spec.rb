require 'aws_better/commands/config'

RSpec.describe AwsBetter::Commands::Config do
  it "executes `config` command successfully" do
    output = StringIO.new
    options = {}
    command = AwsBetter::Commands::Config.new(options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
