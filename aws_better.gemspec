
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "aws_better/version"

Gem::Specification.new do |spec|
  spec.name          = "aws_better"
  spec.license       = "MIT"
  spec.version       = AwsBetter::VERSION
  spec.authors       = ["Jason"]
  spec.email         = ["jaciones@gmail.com"]

  spec.summary       = %q{Tool to improve command line aws functionality}
  spec.description   = %q{Better aws functionality}
  spec.homepage      = "http://www.blitline.com/aws_better"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "http://www.blitline.com/aws_better/src"
    spec.metadata["changelog_uri"] = "http://www.blitline.com/aws_better/changelog"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "tty"
	spec.add_dependency "tty-table"
  spec.add_dependency "tty-prompt"
  spec.add_dependency "pastel"
  spec.add_dependency "thor"
	spec.add_dependency "aws-sdk-cloudwatch"
  spec.add_dependency "aws-sdk-cloudwatchlogs"
  spec.add_dependency "aws-sdk-ecs"
  spec.add_dependency "aws-sdk-states"
	spec.add_dependency "aws-sdk-ec2"
  spec.add_dependency "aws-sdk-lambda"
  spec.add_dependency "dotiw"
  spec.add_dependency "actionview"
  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
